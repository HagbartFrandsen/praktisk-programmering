#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int orbit_diff_equation
(double t, const double u[], double dudt[], void * params)
{
double EPS=*(double *)params;
	dudt[0]=u[1];
	dudt[1]=1-u[0]+EPS*u[0]*u[0];
return GSL_SUCCESS;
}

double orbit_function(double x, double EPS, double u1, double u2){
	gsl_odeiv2_system sys;
	sys.function = orbit_diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = &EPS;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[2]={u1,u2};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int odeiv_diff_equation
(double t, const double y[], double dydt[], void * params)
{
	dydt[0]=y[0]*(1-y[0]);
return GSL_SUCCESS;
}

double odeiv_function(double x){
	gsl_odeiv2_system sys;
	sys.function = odeiv_diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[1]={0.5};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(){
	double a=0,b=10,dx=0.1,EPS=0,u1=1,u2=0,u3=1,u4=-0.5,EPS0=0.01;
	for(double x=a;x<=b;x+=dx)printf("%g %g %g %g %g %g\n",x,odeiv_function(x),exp(x)/(1+exp(x)),orbit_function(x,EPS,u1,u2),orbit_function(x,EPS,u3,u4),orbit_function(x,EPS0,u3,u4));

return 0;
}
	
