#include "stdio.h"
#include "math.h"
#include "complex.h"
int main()
{
double a=tgamma(5);
double b=j0(0.5);
double complex c=sqrt(-2);
double complex d=exp(I);
double complex e=exp(I*acos(-1));
double complex f=cpow(I,exp(1));
printf("%g\n", a);
printf("%g\n", b);
printf("%f+i%f\n", creal(c), cimag(c));
printf("%f+i%f\n", creal(d), cimag(d));
printf("%f+i%f\n", creal(e), cimag(e));
printf("%f+i%f\n", creal(f), cimag(f));

int A=0.1111111111111111111111111111111111111111111111111111;
float B=0.1111111111111111111111111111111111111111111111111111;
double C=0.1111111111111111111111111111111111111111111111111111;
long double D=0.1111111111111111111111111111111111111111111111111111L;
printf("%.25i\n", A);
printf("%.25g\n", B);
printf("%.25lg\n", C);
printf("%.25Lg\n", D);
return 0;
}

