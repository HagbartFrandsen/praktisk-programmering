#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"
#include<math.h>
#include<float.h>

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v); v=NULL; }

void nvector_set(nvector* v, int i, double value){ (*v).data[i]=value; }

double nvector_get(nvector* v, int i){return (*v).data[i]; }

int double_equal(double a,double b)
{
if(a==b)
	{
	return 1;
	}
	else
	{
	return 0;
	}
}

void nvector_add(nvector* a, nvector* b){
int SIZE=sizeof(a);
	for(int i=0; i<SIZE; i++)
	{
	(*a).data[i]=(*a).data[i]+(*b).data[i];
	}
}

void nvector_print(char* s, nvector* v)
{
printf(s);
int SIZE=sizeof(v);
double w;
for(int i=0; i<SIZE; i++)
	{
	w=(*v).data[i];
	printf("%g ,",w);
	}
printf("\n");
}



int nvector_equal(nvector* a, nvector* b)
{
int SIZE=sizeof(a);
int RESULT;
	for(int i=0; i<SIZE; i++)
	{
		if(fabs((*a).data[i]-(*b).data[i])<DBL_EPSILON)
		{
			if(fabs((*a).data[i]-(*b).data[i])/(fabs((*a).data[i])+fabs((*b).data[i]))<DBL_EPSILON)
			{
			RESULT=RESULT+0;
			}
			else
			{
			RESULT=RESULT+1;
			}
		}
		else
		{
		RESULT=RESULT+1;
		}
	if(RESULT==0)
	{
	return 1;
	}
	else
	{
	return 0;
	}
	}
}

