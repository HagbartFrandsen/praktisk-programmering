#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<assert.h>

int
print_state (size_t iter, gsl_multiroot_fsolver * s)
{
  printf ("iter = %3u x = % .3f % .3f "
          "f(x) = % .3e % .3e\n",
          iter,
          gsl_vector_get (s->x, 0), 
          gsl_vector_get (s->x, 1),
          gsl_vector_get (s->f, 0), 
          gsl_vector_get (s->f, 1));
}

struct rparams
  {
    double a;
    double b;
  };

int rosenbrock_f (const gsl_vector * x, void *params, gsl_vector * f)
{
  double a = ((struct rparams *) params)->a;
  double b = ((struct rparams *) params)->b;

  const double x0 = gsl_vector_get (x, 0);
  const double x1 = gsl_vector_get (x, 1);

  const double y0 = -2*(a-x0)-4*b*x0*(x1-pow(x0,2));
  const double y1 = 2*b*(x1-pow(x0,2));

  gsl_vector_set (f, 0, y0);
  gsl_vector_set (f, 1, y1);

  return GSL_SUCCESS;
}

/* opgave 2 */

int Hydrogen_diff_equation
(double r, const double f[], double dfdr[], void * params)
{ /* vores diffenrential ligning -(1/2)f'' -(1/r)f = εf */
double E=*(double *)params;
	dfdr[0]=f[1];
	dfdr[1]=-2*E*f[0]-2*pow(r,-1)*f[0];
return GSL_SUCCESS;
}

double Hydrogen_function(double x, double E, double u1, double u2){
	assert(E<0); 
	assert(x>=0); 
	const double xmin=1e-3;
	if(x<xmin)return x-pow(x,2); /* f(r --> 0)=y0(r --> 0)=r-r^2 */


	gsl_odeiv2_system sys;
	sys.function = Hydrogen_diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*)&E;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double x_run=xmin,y[2]={x_run-pow(x_run,2),1-2*x_run};
	gsl_odeiv2_driver_apply(driver,&x_run,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int
main (void)
{
  const gsl_multiroot_fsolver_type *T;
  gsl_multiroot_fsolver *s;

  int status;
  size_t i, iter = 0;

  const size_t n = 2;
  struct rparams p = {1.0, 100.0};
  gsl_multiroot_function f = {&rosenbrock_f, n, &p};

  double x_init[2] = {0.5, 2.0};
  gsl_vector *x = gsl_vector_alloc (n);

  gsl_vector_set (x, 0, x_init[0]);
  gsl_vector_set (x, 1, x_init[1]);

  T = gsl_multiroot_fsolver_hybrids;
  s = gsl_multiroot_fsolver_alloc (T, 2);
  gsl_multiroot_fsolver_set (s, &f, x);

  print_state (iter, s);

  do
    {
      iter++;
      status = gsl_multiroot_fsolver_iterate (s);

      print_state (iter, s);

      if (status)   /* check if solver is stuck */
        break;

      status = 
        gsl_multiroot_test_residual (s->f, 1e-7);
    }
  while (status == GSL_CONTINUE && iter < 1000);

  printf ("status = %s\n", gsl_strerror (status));

  gsl_multiroot_fsolver_free (s);
  gsl_vector_free (x);

printf("\n\n");

	/* opgave 2 */
double a=0,b=8,dx=0.1,EPS=-0.5,u1=0,u2=0;
	for(double x=a;x<=b;x+=dx)printf("%g %g\n",x,Hydrogen_function(x,EPS,u1,u2));


  return 0;
}


