#include<limits.h>
#include<float.h>
#include<stdio.h>
#include<math.h>

/* opg 3 */
int equal(double a,double b,double tau,double epsilon)
{
if(fabs(a-b)<tau)
{
	if(fabs(a-b)/(fabs(a)+fabs(b))<epsilon)
	{
	return 1;
	}
	else
	{
	return 0;
	}

}
else
{
return 0;
}

}


int main()
{
/* opg 1.1 */
int i=1;
while(i+1>i) 
{
i++;
}
printf("while loop my max int = %i\n", i);

i=1;
do {
i++;
} while(i<i+1);
printf("do while loop my max int = %i\n", i);

i=1;
for(i=1;i<i+1;i++) 
{
i++;
}
printf("for loop my max int = %i\n", i);

printf("real max = %i\n", INT_MAX);

/* opg 1.2 */
i=-1;
while(i-1<i) 
{
i--;
}
printf("while loop my min int = %i\n", i);

i=-1;
do {
i--;
} while(i>i-1);
printf("do while loop my min int = %i\n", i);

i=-1;
for(i=1;i+1>i;i--) 
{
i--;
}
printf("for loop my min int = %i\n", i);

printf("real min = %i\n", INT_MIN);

/* opg 1.3 */
printf("testing of DBL_EPSILON = %g\n",DBL_EPSILON);
double d=1; 
while(1+d!=1){d/=2;} d*=2; printf("double while = %g\n",d);

for(d=1; 1+d!=1; d/=2){} d*=2; printf("double for = %g\n",d);
d=1;
do{d/=2;} while(1+d!=1); d*=2; printf("double do while = %g\n",d); 

printf("testing of FLT_EPSILON = %g\n",FLT_EPSILON);
float f=1; 
while(1+f!=1){f/=2;} f*=2; printf("float while = %g\n",f);

for(f=1; 1+f!=1; f/=2){} f*=2; printf("float for = %g\n",f);
f=1;
do{f/=2;} while(1+f!=1); f*=2; printf("float do while = %g\n",f); 

printf("testing of LDBL_EPSILON = %Lg\n",LDBL_EPSILON);
long double ld=1; 
while(1+ld!=1){ld/=2;} ld*=2; printf("long double while = %Lg\n",ld);

for(ld=1; 1+ld!=1; ld/=2){} ld*=2; printf("long double for = %Lg\n",ld);
ld=1;
do{ld/=2;} while(1+ld!=1); ld*=2; printf("long double do while = %Lg\n",ld); 

/* opg 2 */
int max=INT_MAX/1000;

float F, sum_up_float, sum_down_float;

for(F=1; F<=max; F++){sum_up_float+=1.0f/F;}
printf("sum_up_float=%g\n",sum_up_float);

for(F=max; F>=1; F--){sum_down_float+=1.0f/F;}
printf("sum_down_float=%g\n",sum_down_float);

printf("I sum_up kommer de største led i summen først, mens de største led kommer sidst i sum_down. Programmet kan ikke holde styr på enormt mange led, og dermed afrundes nogle af de sidste led. Sum_up bliver dermed størst.\n");

printf("sum(1/n) er en velkendt divegent række, så den konvergere altså ikke.\n");

double D, sum_up_double, sum_down_double;

for(D=1; D<=max; D++){sum_up_double+=1.0f/D;}
printf("sum_up_float=%g\n",sum_up_double);

for(D=max; D>=1; D--){sum_down_double+=1.0f/D;}
printf("sum_down_float=%g\n",sum_down_double);

printf("For double er der flere decimaler, og de er derfor tætere på hinanden end float er.\n");

double a0=1,a1=1;
int svar=equal(a0,a1,DBL_EPSILON,DBL_EPSILON);
printf("Er %g lig %g?\n",a0,a1);

if(svar==1)
	{
	printf("ja.\n");
	}
	else
	{
	printf("nej.\n");
	}

return 0;
}
