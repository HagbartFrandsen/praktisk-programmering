struct komplex {double re, im;};
typedef struct komplex komplex;

#include <stdio.h>      /* Standard Library of Input and Output */
#include <complex.h>    /* Standard Library of Complex Numbers */
void komplex_set(komplex *z, double x, double y){
	(*z).re = x;
	(*z).im = y;
	}
