struct komplex {double re, im;};
typedef struct komplex komplex;

komplex komplex_new(double x , double y )
{
komplex result = {.re=x, .im=y};
return result;
}
