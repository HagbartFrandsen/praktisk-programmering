struct komplex {double re, im;};
typedef struct komplex komplex;

komplex komplex_conjugate(komplex a){
	double x = a.re, y = -a.im;
	komplex result = {.re = x, .im = y};
	return result;
	}
