struct komplex {double re, im;};
typedef struct komplex komplex;

komplex komplex_add(komplex a, komplex b){
	double x = a.re + b.re, y = a.im + b.im;
	komplex result = {.re = x, .im = y};
	return result;
	}
