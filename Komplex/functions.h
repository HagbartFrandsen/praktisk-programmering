#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<math.h>

struct komplex {double re, im;};
typedef struct komplex komplex;

void komplex_set(komplex *z, double x, double y);
komplex komplex_add(komplex a, komplex b);
komplex komplex_conjugate(komplex a);
komplex komplex_sub(komplex a, komplex b);
komplex komplex_new(double a, double y );
int komplex_equal(komplex a, komplex b, double tau, double eps);
double komplex_abs(komplex z);
void komplex_print(char* s, komplex z);
