#include<stdio.h>

struct komplex {double re, im;};
typedef struct komplex komplex;

void komplex_print(char *s, komplex a){
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}
