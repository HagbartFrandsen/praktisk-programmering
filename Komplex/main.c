#include<stdio.h>
#include<limits.h>
#include<float.h>
#include"functions.h"
#include<math.h>

int main(void)
{
komplex a = {1,2}, b = {3,4};
komplex r = komplex_add(a,b);
komplex R = {4,6};
komplex_print(" ",a);
printf("+\n");
komplex_print(" ",b);
komplex_print(" = ",r);
komplex_print("add should be",R);

komplex ac = {1,2};
komplex rc = komplex_conjugate(ac);
komplex Rc = {1,-2};
komplex_print("conjugate",ac);
komplex_print(" =",rc);
komplex_print("conjugate should be",Rc);

komplex as = {1,2}, bs = {3,4};
komplex rs = komplex_sub(a,b);
komplex Rs = {-2,-2};
komplex_print(" ",as);
printf(" - \n");
komplex_print(" ",bs);
komplex_print(" = ",rs);
komplex_print("sub should be",Rs);

double x=1;
double y=2;

komplex zn=komplex_new(x,y);
komplex_print("new should be",zn);

komplex z={3,2};
void komplex_set(komplex *z,double x,double y);
komplex_print("set should be",z);


komplex_print("print something", zn);
/* the following is optional  */

/*#define TINY 1e-6
if( komplex_equal(R,r,TINY,TINY) ) printf("test 'add' passed :) \n");
else printf("test 'add' failed: debug me, please... \n");*/


return 0;
}
