#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf_ellint.h>
const double Pi = 3.14159265358979323844;


double integ (double t, void * params) {
	double k = *(double *) params;
	double integ = pow( sqrt( 1 - pow( k , 2 ) * pow( sin( t ) , 2 ) ) , -1 );
return integ;
}

int
main (void)
{
	double gslresult,result,abserr,epsabs=1e-13,epsrel=1e-13,k,dk=0.25,phi,dphi=0.1;
	gsl_mode_t E=1e-13;
	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);

/* preparing for k,F(phi,k) plot */
for (k=0;k<1;k=k+dk){ 
	printf ("\n\nk phi result gslresult error\n");
	for (phi=-Pi;phi<Pi;phi=phi+dphi){
		gsl_function F;
		F.function = &integ;
		F.params = &k;

		gsl_integration_qags (&F, 0, phi, epsabs, epsrel, 1000, w, &result, &abserr); 

		gslresult = gsl_sf_ellint_F(phi,k,E);

		printf ("% .18f % .18f % .18f % .18f % .18f\n", k, phi, result, gslresult, gslresult-result);
	}
}

/* giving special treatement for the k=1 case */
k=1;
printf ("\n\nk phi result gslresult error\n");
	for (phi=-Pi/2;phi<Pi/2;phi=phi+dphi){
		if( 1 - pow( k , 2 ) * pow( sin( phi ) , 2 ) >0 ){
			gsl_function F;
			F.function = &integ;
			F.params = &k;

			gsl_integration_qags (&F, 0, phi, epsabs, epsrel, 1000, w, &result, &abserr); 

			gslresult = gsl_sf_ellint_F(phi,k,E);

			printf ("% .18f % .18f % .18f % .18f % .18f\n", k, phi, result, gslresult, gslresult-result);
			}
	}

/* preparing for phi,F(phi,k) plot */
dk=0.01,dphi=Pi/2;
for (phi=-Pi;phi<=Pi;phi=phi+dphi){ 
	printf ("\n\nk phi result gslresult error\n");
	for (k=-1+dk;k<1;k=k+dk){
		gsl_function F;
		F.function = &integ;
		F.params = &k;

		gsl_integration_qags (&F, 0, phi, epsabs, epsrel, 1000, w, &result, &abserr); 

		gslresult = gsl_sf_ellint_F(phi,k,E);

		printf ("% .18f % .18f % .18f % .18f % .18f\n", k, phi, result, gslresult, gslresult-result);
	}
}
gsl_integration_workspace_free (w);
return 0;
}

