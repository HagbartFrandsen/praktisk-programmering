#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_linalg.h>

/* definitioner */
const double pi = 3.14159;

int main(){

/* opg 1 */
int N=3;
gsl_matrix *A = gsl_matrix_calloc(N,N);
  gsl_matrix_set(A,0,0,6.13);
  gsl_matrix_set(A,0,1,-2.90);
  gsl_matrix_set(A,0,2,5.86);
  gsl_matrix_set(A,1,0,8.08);
  gsl_matrix_set(A,1,1,-6.31);
  gsl_matrix_set(A,1,2,-3-89);
  gsl_matrix_set(A,2,0,-4.36);
  gsl_matrix_set(A,2,1,1.00);
  gsl_matrix_set(A,2,2,0.19);

gsl_vector *x = gsl_vector_calloc(N);
gsl_vector *b = gsl_vector_calloc(N);
  gsl_vector_set(b,0,6.23);
  gsl_vector_set(b,1,5.37);
  gsl_vector_set(b,2,2.29);

int gsl_linalg_HH_solve (gsl_matrix * A, const gsl_vector * b, gsl_vector * x);
fprintf(stderr,"x=(%g, %g, %g)",gsl_vector_get(x, 0),gsl_vector_get(x,1),gsl_vector_get(x, 2));
/* opg 1 færdig */

/* opg 2 a */
int n=20;
double s=1.0/(n+1);
gsl_matrix *H = gsl_matrix_calloc(n,n); /* laver en n gange n matrix af nuller */
for(int i=0;i<n-1;i++){ /* en for funktion til at ændre diagonalerne */
  gsl_matrix_set(H,i,i,-2);
  gsl_matrix_set(H,i,i+1,1);
  gsl_matrix_set(H,i+1,i,1);
  }
gsl_matrix_set(H,n-1,n-1,-2); /* sætter det sidte led i diagonalen til -2, da der ikke var plads til at gøre det i forloopet */
gsl_matrix_scale(H,-1/s/s); /* scallere hele matricen med -1/s² */
/* opg 2 a færdig */

/* opg 2 b */
gsl_eigen_symmv_workspace* w =  gsl_eigen_symmv_alloc (n); /* gsl_eigen_symm_workspace * gsl_eigen_symm_alloc (const size_t n): laver en fri plads til af finde egenværdier for en n gange n reel symmetrisk matrix */ 
gsl_vector* eval = gsl_vector_alloc(n); /* allokere plads til en n vector, altså den "reservere" plads til den */
gsl_matrix* evec = gsl_matrix_calloc(n,n); /* laver n, n vectorer med nuller i alle indgange */
gsl_eigen_symmv(H,eval,evec,w); /* udregner egenværdierne for H og putter dem ind i den fri plads vi lavede tidligere i w og laver en liste med dem i eval, og egenvektorerne i evec. Vi skal have en tom w, for at denne funktion kan køre */
/* opg 2 b færdig */

/* opg 2 c */
gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC); /* sortere egenværdierne i eval, og de tilhørende egenvektorer i evec, efter størrelse på egenværdierne. */
/* opg 2 c færdig */

/* opg 2 d */
fprintf (stderr, "\n\ni exact calculated\n");
for (int k=0; k < n/3; k++){
    double exact = pi*pi*(k+1)*(k+1);
    double calculated = gsl_vector_get(eval,k);
    fprintf (stderr, "%i   %g   %g\n", k, exact, calculated); /* printer plads k, udregnet egenværdi/energi, og den egentlige egenværdi/energi som vi ved fra teori. */
}
/* opg 2 d færdig */



/* opg 2 e */
fprintf(stderr,"\n\nx u0 u1 u2\n");
	fprintf(stderr,"%f %f %f %f\n", -0., 0., 0., 0.);
gsl_vector *xvec = gsl_vector_alloc(19);
	double step = (0- 37) / (19 + 1);
for (int i = 0; i < 19; i++)
{
		gsl_vector_set(xvec, i, 0 - (i + 1) * step);
}
	for (int i = 0; i < 19; i++) {
		double x = gsl_vector_get(xvec, i);
		double u0 = gsl_matrix_get(evec, i, 0);
		double u1 = gsl_matrix_get(evec, i, 1);
		double u2 = gsl_matrix_get(evec, i, 2);
		fprintf(stderr,"%f %f %f %f\n", x, u0, u1, u2);
	}
	fprintf(stderr,"%f %f %f %f\n\n\n", 37., 0., 0., 0.);
for(int k=0;k<3;k++){
  fprintf(stderr,"%g %g\n",0.0,0.0);
  for(int i=0;i<n;i++) fprintf(stderr,"%g %g\n",(i+1.0)/(n+1),gsl_matrix_get(evec,i,k));
  fprintf(stderr,"%g %g\n",1.0,0.0);
  fprintf(stderr,"\n\n");
  }
/* opg 2 e færdig */

return 0;
} 
