#include<stdio.h>
#include<limits.h>
#include<float.h>

int main()
{

double x=1;
while(1+x!=1)
{
x/=2;
}
x*=2;
printf("%g\n", x);

x=1;
do {
x/=2;
} while(1+x!=1);
x*=2;
printf("%g\n", x);

for(x=1; 1+x!=1; x/=2){
} 
x*=2;

printf("%g\n", x);
printf("%g\n", DBL_EPSILON);

long double z=1;
while(1+z!=1)
{
z/=2;
}
z*=2;
printf("%Lg\n", z);

z=1;
do {
z/=2;
} while(1+z!=1);
printf("%Lg\n", z);

for(z=1; 1+z!=1; z/=2){
} 
z*=2;

printf("%Lg\n", z);
printf("%Lg\n", LDBL_EPSILON);


float y=1;
while(1+y!=1)
{
y/=2;
}
y*=2;
printf("%f\n", y);

y=1;
do {
y/=2;
} while(1+y!=1);
printf("%f\n", y);

for(y=1; 1+y!=1; y/=2){
} 
y*=2;

printf("%f\n", y);
printf("%f\n", FLT_EPSILON);

return 0;
}
