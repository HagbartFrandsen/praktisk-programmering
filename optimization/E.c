#include<math.h>
#include<assert.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double f(double x, void* params){
	double *p=(double*)params;
	double y=p[0];
	double f=pow(1-x,2)+100*pow(y-pow(x,2),2);
	return f;
}

double E(double y){
	/*assert(x>0 && y>0);*/
	double p[1] = {y};

	gsl_function F;
	F.function = f;
	F.params = (void*)p;

	int limit = 100;
	double r=0,acc=1e-1,eps=1e-1,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);

int status;
status=gsl_integration_qagiu(&F,r,acc,eps,limit,workspace,&result,&err);

gsl_integration_workspace_free(workspace);
if(status!=GSL_SUCCESS) return NAN;
else return result;
}
