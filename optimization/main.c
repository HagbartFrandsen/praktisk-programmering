#define ALG gsl_multimin_fminimizer_nmsimplex2
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<math.h>

struct experimental_data {int n; double *t,*y,*e;};

double function_to_minimize (const gsl_vector *x2, void *params) {
	double  A = gsl_vector_get(x2,0);
	double  T = gsl_vector_get(x2,1);
	double  B = gsl_vector_get(x2,2);
	struct experimental_data *p = (struct experimental_data*) params;
	int     n = p->n;
	double *t = p->t;
	double *y = p->y;
	double *e = p->e;
	double sum=0;
	#define f(t) A*exp(-(t)/T) + B
	for(int i=0;i<n;i++){ sum = pow( (f(t[i]) - y[i] )/e[i] ,2);}
	return sum;
}

double my_f (const gsl_vector * r, void * params){
	double x=gsl_vector_get(r,0);
	double y=gsl_vector_get(r,1);
	double robert=pow(1-x,2)+100*pow(y-pow(x,2),2);
	return robert;
}

int 
main(void)
{

  const gsl_multimin_fminimizer_type *T = 
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;

  /* Starting point */
  x = gsl_vector_alloc (2);
  gsl_vector_set (x, 0, 2);
  gsl_vector_set (x, 1, 3);

  /* Set initial step sizes to 1 */
  ss = gsl_vector_alloc (2);
  gsl_vector_set_all (ss, 1.0);

  /* Initialize method and iterate */
  minex_func.n = 2;
  minex_func.f = my_f;
  minex_func.params = NULL;

  s = gsl_multimin_fminimizer_alloc (T, 2);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);

  fprintf (stderr,"Problem 1:\n");
  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);
      
      if (status) 
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);

      if (status == GSL_SUCCESS)
        {
          printf ("converged to minimum at\n");
        }
      fprintf (stderr,"%5d %10.3e %10.3e f() = %7.3f size = %.3f\n", 
              iter,
              gsl_vector_get (s->x, 0), 
              gsl_vector_get (s->x, 1), 
              s->fval, size);
    }
  while (status == GSL_CONTINUE && iter < 100);
  
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);

	/*opgave 2*/

double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
int n = sizeof(t)/sizeof(t[0]);

	fprintf (stderr,"\n\n");
	fprintf (stderr,"Problem 2:\n");
	fprintf (stderr,"\n\n");

	fprintf(stderr,"t \t y \t e \n");
for(int i=0;i<n;i++){
	fprintf(stderr,"%g \t %g \t %g\n",t[i],y[i],e[i]);
}
	fprintf(stderr,"\n\n");

struct experimental_data params;
	params.n=n;
	params.t=t;
	params.y=y;
	params.e=e;

size_t dim2=3;
	gsl_multimin_function F2;
	F2.f=function_to_minimize;
	F2.n=dim2;
	F2.params=(void*)&params;

gsl_multimin_fminimizer * state2 =
gsl_multimin_fminimizer_alloc (ALG,dim2);
gsl_vector *start2 = gsl_vector_alloc(dim2);
gsl_vector *step2 = gsl_vector_alloc(dim2);
gsl_vector_set(start2,0,2); /* A_start */
gsl_vector_set(start2,1,2); /* T_start */
gsl_vector_set(start2,2,2); /* B_start */
gsl_vector_set_all(step2,0.1);
gsl_multimin_fminimizer_set (state2, &F2, start2, step2);

fprintf(stderr,"Least square fit data: \n\n\n");

int iter2=0,status2;
double acc2=0.001;
do{
	iter2++;
	int flag2 = gsl_multimin_fminimizer_iterate (state2);
	if(flag2!=0)break;
	status2 = gsl_multimin_test_size (state2->size, acc2);
	if (status2 == GSL_SUCCESS) fprintf (stderr,"converged\n");
	fprintf(stderr,
		"iter=%2i, A= %8f, T= %8f, B= %8f, F= %8g, size= %8g\n",
		iter2,
		gsl_vector_get(state2->x,0),
		gsl_vector_get(state2->x,1),
		gsl_vector_get(state2->x,2),
		state2->fval,
		state2->size);
}while(status2 == GSL_CONTINUE && iter2 < 99);
	
fprintf(stderr,"The least square fit values of A, T and B are now found.\n");
fprintf(stderr,"In the next index the value of the function for t between 0 and 9 is printed\n\n\n");

double A=gsl_vector_get(state2->x,0);
double T2=gsl_vector_get(state2->x,1);
double B=gsl_vector_get(state2->x,2);

fprintf(stderr,"t \t f(t)=A*exp(-t/T)+B\n");
for(double t=0; t<9; t+=0.05){
fprintf(stderr,"%g \t %g\n",t,A*exp(-t/T2)+B);
}


gsl_vector_free(start2);
gsl_vector_free(step2);
gsl_multimin_fminimizer_free(state2);
}
