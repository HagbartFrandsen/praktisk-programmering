#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double integ (double x, void * params) {
	double integ = log(x) / sqrt(x);
return integ;
}

double Hos (double x, void * params){
	double alpha = *(double *) params;
	double Hos = (-pow(alpha,2)*pow(x,2)/2+alpha/2+pow(x,2)/2)*exp(-alpha*pow(x,2));
return Hos;
}

double norm (double x, void * params){
	double alpha = *(double *) params;
	double norm = exp(-alpha*pow(x,2));
return norm;
}

int
main (void)
{
	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
  
	double result, error;
	double expected = -4.0;

	gsl_function F;
	F.function = &integ;
	F.params = NULL;

	gsl_integration_qags (&F, 0, 1, 0, 1e-7, 1000, w, &result, &error); 

	printf ("result          = % .18f\n", result);
	printf ("exact result    = % .18f\n", expected);
	printf ("estimated error = % .18f\n", error);
	printf ("actual error    = % .18f\n", result - expected);
	printf ("intervals       = %zu\n\n\n", w->size);

	gsl_integration_workspace_free (w);

/* del 2 */

	double alpha,dalpha=0.001;

for (alpha=0.01;alpha<3;alpha=alpha+dalpha){

	gsl_integration_workspace * W = gsl_integration_workspace_alloc (1000);
  
	double Result, Error;

	gsl_function H;
	H.function = &Hos;
	H.params = &alpha;

	gsl_integration_qagi (&H, 0, 1e-7, 1000, W, &Result, &Error); 

	gsl_integration_workspace_free (W);

	gsl_integration_workspace * W2 = gsl_integration_workspace_alloc (1000);
  
	double Result2, Error2;

	gsl_function N;
	N.function = &norm;
	N.params = &alpha;

	gsl_integration_qagi (&N, 0, 1e-7, 1000, W2, &Result2, &Error2); 

	printf ("%g %g %g %g\n",alpha, Result, Result2, Result/Result2);

	gsl_integration_workspace_free (W2);
}

return 0;
}

